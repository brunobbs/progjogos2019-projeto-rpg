
return {
  name = "Blue Slime",
  appearance = 'blue_slime',
  max_hp = 16,
  defense = 10,
  attack = 10,
  speed = 10,
  priority = {'>', 'defense'},
  accuracy = 1,
  crit = 0.1,
}

