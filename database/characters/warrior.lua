
return {
  name = "Veteran Warrior",
  appearance = 'knight',
  max_hp = 20,
  defense = 10,
  attack = 10,
  speed = 2,
  priority = {'>', 'hp'},
  accuracy = 0.9,
  crit = 0.1,
}

