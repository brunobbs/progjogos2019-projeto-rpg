
return {
  name = "Recluse Archer",
  appearance = 'archer',
  max_hp = 12,
  defense = 10,
  attack = 10,
  speed = 11,
  priority = {'>', 'speed'},
  accuracy = 0.7,
  crit = 0.9,
  skill = { name = 'Double Arrow',
            target = 'enemies',
            target_n = '2',
            effect = -1 }
}

