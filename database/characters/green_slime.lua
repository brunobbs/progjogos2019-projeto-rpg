
return {
  name = "Green Slime",
  appearance = 'slime',
  max_hp = 6,
  defense = 10,
  attack = 1,
  speed = 9,
  priority = {'>', 'speed'},
  accuracy = 1,
  crit = 0.4,
}

