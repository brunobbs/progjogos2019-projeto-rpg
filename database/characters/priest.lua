
return {
  name = "Friendly Priest",
  appearance = 'priest',
  max_hp = 8,
  defense = 10,
  attack = 10,
  speed = 10,
  priority = {'<', 'speed'},
  accuracy = 1,
  crit = 0.5,
  skill = { name = 'Heal',
            target = 'heroes',
            target_n = 'all',
            effect = 1 }
}

