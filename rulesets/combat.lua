return function(ruleset)

    local filter = require 'common.filter'

    -- "Alias" variable
    local r = ruleset.record

    -- Defining a trait rule
    function ruleset.define:can_attack(e, target)
        function self.when() return true end

        function self.apply()
            if r:is(e, 'hero') then
                return r:is(target, 'enemy')
            elseif r:is(e, 'enemy') then
                return r:is(target, 'hero')
            end
        end
    end

    function ruleset.define:attack(e, target)
        function self.when() return e:can_attack(target) end

        function self.apply()
            return e:damage(target)
        end
    end

    function ruleset.define:damage(e, target)
        function self.when() return true end

        function self.apply()
            local multiplier = 1
            local result = 2

            if math.random() <= e.crit[1] then
                multiplier = 1.5
                result = 3
            end

            if math.random() >= e.accuracy[1] then
                multiplier = 0
                result = 1
            end

            local damage = (e:get_attack()[1] - 0.5 * target.get_defense()[1]) * multiplier
            if damage < 0 then damage = -1 * damage end

            target:set_hp({target:get_hp()[1] - damage, target:get_hp()[2]})

            if target:get_hp()[1] <= 0 then
                print(target.name .. ' was slain')
            end

            -- this returns 1 if failed to attack, 2 if attack and 3 if critical attack
            return result
        end
    end

    function ruleset.define:get_alive()
        function self.when() return true end

        function self.apply()
            return filter(r:all('character'),
                          function(char)
                return char:get_hp()[1] > 0
            end)
        end
    end

    function ruleset.define:get_alive_enemies()
        function self.when() return true end

        function self.apply()
            return filter(r:all('enemy'),
                          function(enemy)
                return enemy:get_hp()[1] > 0
            end)
        end
    end

    function ruleset.define:get_alive_heroes()
        function self.when() return true end

        function self.apply()
            return filter(r:all('hero'),
                          function(hero)
                return hero:get_hp()[1] > 0
            end)
        end
    end
end
