return function(ruleset)

  -- "Alias" variable
  local r = ruleset.record

  function ruleset.define:is_skilled(e)
    function self.when() return r:is(e, 'character') end
    function self.apply() return r:is(e, 'skilled') end
  end

  function ruleset.define:get_skill(e)
    function self.when() return r:is(e, 'skilled') end
    function self.apply() return r:get(e, 'skilled') end
  end

  function ruleset.define:get_skill_name(e)
    function self.when() return r:is(e, 'skilled') end
    function self.apply() return r:get(e, 'skilled', 'name') end
  end

  function ruleset.define:get_skill_targets(e)
    function self.when() return r:is(e, 'skilled') end
    function self.apply() return r:get(e, 'skilled', 'target') end
  end

  function ruleset.define:get_skill_targets_n(e)
    function self.when() return r:is(e, 'skilled') end
    function self.apply()
      local n = r:get(e, 'skilled', 'target_n')
      if n ~= 'all' then
        n = tonumber(n)
      end
      return n
    end
  end

  function ruleset.define:unleash_skill(e, targets, n)
    function self.when() return r:is(e, 'skilled') end

    function self.apply()
      if n == 'all' then n = #targets end
      for i=1,n,1 do
        e:skill_effect(targets[i])
      end
    end
  end

  function ruleset.define:skill_effect(e, target)
    function self.when() return r:is(target, 'character') end
    function self.apply()
      local effect = r:get(e, 'skilled', 'effect')
      return target:set_hp({target:get_hp()[1] + effect, target:get_hp()[2]})
    end
  end
end
