return function(ruleset)

    local Vec = require 'common.vec'

    -- "Alias" variable
    local r = ruleset.record

    r:new_property('drawable', { position = Vec(), sprite_id = 0 })

    function ruleset.define:make_drawable(e, pos)
        function self.when() return true end

        function self.apply()
            return r:set(e, 'drawable', { position = pos, sprite_id = e:get_appearance() })
        end
    end

    function ruleset.define:get_sprite_id(e)
        function self.when() return r:is(e, 'drawable') end

        function self.apply()
            return r:get(e, 'drawable', 'sprite_id')
        end
    end

    function ruleset.define:get_position(e)
        function self.when() return r:is(e, 'drawable') end

        function self.apply()
            return r:get(e, 'drawable', 'position')
        end
    end
end
