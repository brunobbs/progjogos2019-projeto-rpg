return function(ruleset)

    -- "Alias" variable
    local r = ruleset.record

    r:new_property('character', {
        hp = {1, 1},
        appearance = '',
        attack = {1, 1},
        defense = {1, 1},
        speed = {1, 1},
        priority = {'<', 'hp'},
        accuracy = {0.8, 0.8},
        crit = {0.2, 0.2},
    })
    r:new_property('hero', {})
    r:new_property('enemy', {})
    r:new_property('skilled', {name = '', target = '', target_n = '', effect = 1})

    -- Defining a trait rule
    function ruleset.define:get_hp(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'hp') end
    end

    function ruleset.define:set_hp(e, hp)
        function self.when() return r:is(e, 'character') end
        function self.apply()
            if hp[1] > hp[2] then
                hp[1] = hp[2]
            end
            return r:set(e, 'character', 'hp', hp)
        end
    end

    function ruleset.define:get_appearance(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'appearance') end
    end

    function ruleset.define:get_attack(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'attack') end
    end

    function ruleset.define:get_defense(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'defense') end
    end

    function ruleset.define:get_speed(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'speed') end
    end

    function ruleset.define:get_priority(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'priority') end
    end

    function ruleset.define:get_crit(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'crit') end
    end

    function ruleset.define:get_accuracy(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:get(e, 'character', 'accuracy') end
    end

    function ruleset.define:is_enemy(e)
        function self.when() return r:is(e, 'character') end
        function self.apply() return r:is(e, 'enemy') end
    end

    function ruleset.define:new_character(file, side)
        function self.when() return true end
        function self.apply()
            local e = ruleset:new_entity()

            local spec = require('database.characters.' .. file)

            r:set(e, 'named', {name = spec.name})
            r:set(e, 'character', {
                hp = {spec.max_hp, spec.max_hp},
                appearance = spec.appearance,
                defense = {spec.defense, spec.defense},
                attack = {spec.attack, spec.attack},
                speed = {spec.speed, spec.speed},
                priority = spec.priority
            })
            r:set(e, side, {})

            if spec.skill then
                r:set(e, 'skilled', spec.skill)
            end

            return e
        end
    end
end
