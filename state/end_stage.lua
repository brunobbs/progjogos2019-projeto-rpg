local PALETTE_DB = require 'database.palette'
local State = require 'state'

local EndStageState = require 'common.class' (State)

function EndStageState:_init(stack)
    self:super(stack)
end

function EndStageState:enter(params)
    local EndView = require 'view.text_view'("You " .. params, "Press Escape")
    love.graphics.setBackgroundColor(PALETTE_DB.black)
    self:view('hud'):add('end_text', EndView)
end

function EndStageState:suspend() self:view('hud'):remove('end_text') end

function EndStageState:resume(params)
    local EndView = require 'view.text_view'("You " .. params, "Press Escape")
    self:view('hud'):add('end_text', EndView)
end

function EndStageState:leave() self:view('hud'):remove('end_text') end

function EndStageState:on_keypressed(key)
    if key == 'escape' then
        return self:pop()
    end
end

return EndStageState

