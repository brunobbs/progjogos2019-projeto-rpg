
local Character = require 'model.character'
local State = require 'state'

local FollowQuestState = require 'common.class' (State)

function FollowQuestState:_init(stack)
  self:super(stack)
  self.party = nil
  self.encounters = nil
  self.next_encounter = nil
end

function FollowQuestState:enter(params)
  local quest = params.quest
  self.encounters = quest.encounters
  self.next_encounter = 1
  self.party = {}
  self.rule_engine = params.rule_engine

  for i, character_name in ipairs(quest.party) do
    self.party[i] = self.rule_engine:new_character(character_name, 'hero')
  end
end

function FollowQuestState:update(_)
  if self.next_encounter <= #self.encounters then
    local encounter = {}
    local encounter_specnames = self.encounters[self.next_encounter]
    self.next_encounter = self.next_encounter + 1
    for i, character_name in ipairs(encounter_specnames) do
      encounter[i] = self.rule_engine:new_character(character_name, 'enemy')
    end
    local params = { party = self.party, encounter = encounter, rule_engine = self.rule_engine }
    return self:push('encounter', params)
  else
    return self:pop()
  end
end

return FollowQuestState


