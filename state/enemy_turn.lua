
local Vec = require 'common.vec'
local CharacterStats = require 'view.character_stats'
local TurnCursor = require 'view.turn_cursor'
local ListMenu = require 'view.list_menu'
local State = require 'state'

local sort_spec = require 'common.sort_spec'

local EnemyTurnState = require 'common.class' (State)

local TURN_OPTIONS = { 'Fight', 'Skill', 'Item'}

function EnemyTurnState:_init(stack)
  self:super(stack)
  self.character = nil
  self.menu = ListMenu(TURN_OPTIONS)
end

function EnemyTurnState:enter(params)
  self.character = params.current_character
  self.rule_engine = params.rule_engine
  self:_show_cursor()
  self:_show_stats()
end

function EnemyTurnState:update()
  local option = TURN_OPTIONS[self.menu:current_option()]
  local target = self:get_target(self.character)
  return self:pop({ action = option, character = self.character, target = target})
end

function EnemyTurnState:get_target()
  print("will generate", self.character:get_priority())
  local sort_function = sort_spec(self.character.priority)

  local targets = self.rule_engine:get_alive_heroes()
  table.sort(targets, sort_function)
  return targets[1]
end

function EnemyTurnState:_show_cursor()
  local atlas = self:view():get('atlas')
  local sprite_instance = atlas:get(self.character)
  local cursor = TurnCursor(sprite_instance)
  self:view():add('turn_cursor', cursor)
end

function EnemyTurnState:_show_stats()
  local bfbox = self:view():get('battlefield').bounds
  local position = Vec(bfbox.right + 16, bfbox.top)
  local char_stats = CharacterStats(position, self.character)
  self:view():add('char_stats', char_stats)
end

function EnemyTurnState:leave()
  self:view():remove('turn_menu')
  self:view():remove('turn_cursor')
  self:view():remove('char_stats')
end

return EnemyTurnState

