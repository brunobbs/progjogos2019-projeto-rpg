
local Vec = require 'common.vec'
local MessageBox = require 'view.message_box'
local SpriteAtlas = require 'view.sprite_atlas'
local BattleField = require 'view.battlefield'
local State = require 'state'
local inspect = require 'lib.inspect'

local sort_spec = require 'common.sort_spec'
local sleep = require 'common.sleep'

local EncounterState = require 'common.class' (State)

local CHARACTER_GAP = 96

local MESSAGES = {
  Fight = {"%s failed to attack %s", '%s attacked %s', '%s critical attack on %s'},
  Skill = "%s unleashed the %s skill",
  Item = "%s used an item",
}

function EncounterState:_init(stack)
  self:super(stack)
  self.turns = nil
  self.next_turn = nil
end

function EncounterState:enter(params)
  local atlas = SpriteAtlas()
  local battlefield = BattleField()
  local bfbox = battlefield.bounds
  local message = MessageBox(Vec(bfbox.left, bfbox.bottom + 16))
  local n = 0
  local party_origin = battlefield:east_team_origin()
  self.rule_engine = params.rule_engine
  self.turns = {}
  self.enemies = {}
  self.next_turn = 0
  for i, character in ipairs(params.party) do
    local pos = party_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[i] = character
    atlas:add(character, pos, character:get_appearance())
    n = n + 1
  end
  local encounter_origin = battlefield:west_team_origin()
  for i, character in ipairs(params.encounter) do
    self.enemies[i] = character
    local pos = encounter_origin + Vec(0, 1) * CHARACTER_GAP * (i - 1)
    self.turns[n + i] = character
    atlas:add(character, pos, character:get_appearance())
  end
  self:view():add('atlas', atlas)
  self:view():add('battlefield', battlefield)
  self:view():add('message', message)
  message:set("You stumble upon an encounter")
end

function EncounterState:leave()
  self:view():get('atlas'):clear()
  self:view():remove('atlas')
  self:view():remove('battlefield')
  self:view():remove('message')
end

function EncounterState:update(_)
  local turns = self.rule_engine:get_alive()
  local sort_speed = sort_spec({'>', 'speed'})
  table.sort(turns, sort_speed)
  self.next_turn = self.next_turn % turns.n + 1
  local current_character = turns[self.next_turn]
  local params = { current_character = current_character, rule_engine = self.rule_engine}

  if self.rule_engine:get_alive_enemies().n <= 0 then
    self:pop()
    return self:push('end_stage', 'defeated the monsters')
  end

  if self.rule_engine:get_alive_heroes().n <= 0 then
    self:pop()
    self:pop()
    return self:push('end_stage', 'Lost!')
  end

  if current_character:is_enemy() then
    return self:push('enemy_turn', params)
  end

  return self:push('player_turn', params)
end

function EncounterState:resume(params)
  if params.action ~= 'Run' then
    self:action(params.action, params.character, params.target)
  else
    return self:pop()
  end
end

function EncounterState:action(action, character, target)
  if action == 'Fight' then
    if not character:is_enemy() then
      target = self.rule_engine:get_alive_enemies()[1]
    else
      sleep(0.6) -- wait when enemy attacks
    end

    local result = character:attack(target)
    local message = MESSAGES[action][result]:format(character:get_name(), target:get_name())
    self:view():get('message'):set(message)

  elseif action == 'Skill' then
    if not character:is_skilled() then
      local message = ("%s is not skilled!"):format(character:get_name())
      self:view():get('message'):set(message)
    else
      local targets = self.rule_engine:get_alive_enemies()
      if character:get_skill_targets() == 'heroes' then
        targets = self.rule_engine:get_alive_heroes()
      end

      character:unleash_skill(targets, character:get_skill_targets_n())

      local message = MESSAGES[action]:format(character:get_name(), character:get_skill_name())
      self:view():get('message'):set(message)
    end
  end
end

return EncounterState


