package.path = "lib/?.lua;lib/?/init.lua;" .. package.path

local RULE_MODULES = {'rulesets'}

local RULESETS = {'drawable', 'character', 'combat', 'skilled'}

local rule_engine = require 'ur-proto'(RULE_MODULES, RULESETS)

return rule_engine
