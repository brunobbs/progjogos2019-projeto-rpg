return function (n)  -- seconds

  local clock = os.clock

  local t0 = clock()
  while clock() - t0 <= n do
  end
  
end