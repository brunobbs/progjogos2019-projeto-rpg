-- Tis function generates a sort function based on a spec given
return function(spec)
    return function(a, b)
        if spec[1] == '>' then
            return a[spec[2]][1] > b[spec[2]][1]
        elseif spec[1] == '<' then
            return a[spec[2]][1] < b[spec[2]][1]
        elseif spec[1] == '>=' then
            return a[spec[2]][1] <= b[spec[2]][1]
        elseif spec[1] == '<=' then
            return a[spec[2]][1] >= b[spec[2]][1]
        end
    end
end
