return function (list, func)
    local index = 1
    local filtered = {}
    for _, v in ipairs(list) do
        if func(v) then
            filtered[index] = v
            index = index + 1
        end
    end
    filtered.n = index - 1
    return filtered
end